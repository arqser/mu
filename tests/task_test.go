package tests

import (
	"bytes"
	"encoding/json"
	"mu/internal/server"
	"mu/internal/task"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestTaskShow(t *testing.T) {
	s := server.NewServer()
	req, _ := http.NewRequest("GET", "/tasks/1000", nil)
	rr := httptest.NewRecorder()
	s.Handler.ServeHTTP(rr, req)

	require.Equal(t, http.StatusOK, rr.Code)
	require.Contains(t, rr.Body.String(), "1000")
}

func TestTaskShowNotFound(t *testing.T) {
	s := server.NewServer()
	req, _ := http.NewRequest("GET", "/tasks/9999", nil)
	rr := httptest.NewRecorder()
	s.Handler.ServeHTTP(rr, req)
	require.Equal(t, http.StatusNotFound, rr.Code)
}

func TestTaskCreate(t *testing.T) {
	task := &task.TaskRequest{
		Task: &task.Task{
			Title:   "Title",
			Content: "Content",
		},
	}

	var buf bytes.Buffer
	json.NewEncoder(&buf).Encode(task)
	req, _ := http.NewRequest("POST", "/tasks", &buf)
	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	s := server.NewServer()
	s.Handler.ServeHTTP(rr, req)

	require.Equal(t, http.StatusCreated, rr.Code)
	require.Contains(t, rr.Body.String(), "Title")
}

func TestTaskList(t *testing.T) {
	s := server.NewServer()
	req, _ := http.NewRequest("GET", "/tasks", nil)
	rr := httptest.NewRecorder()
	s.Handler.ServeHTTP(rr, req)

	require.Equal(t, http.StatusOK, rr.Code)
}

func TestTaskEdit(t *testing.T) {
	task := &task.TaskRequest{
		Task: &task.Task{
			Title:   "Updated",
			Content: "Altered content",
		},
	}

	var buf bytes.Buffer
	json.NewEncoder(&buf).Encode(task)
	req, _ := http.NewRequest("PUT", "/tasks/1000", &buf)
	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	s := server.NewServer()
	s.Handler.ServeHTTP(rr, req)

	require.Equal(t, http.StatusOK, rr.Code)
	require.Contains(t, rr.Body.String(), "Updated")
}

func TestTaskDelete(t *testing.T) {
	s := server.NewServer()
	req, _ := http.NewRequest("DELETE", "/tasks/1001", nil)
	rr := httptest.NewRecorder()
	s.Handler.ServeHTTP(rr, req)

	require.Equal(t, http.StatusNoContent, rr.Code)
}
