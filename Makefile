include .env
export

# Build the application
all: build

build:
	@go build -o app cmd/api/main.go

# Run the application
run:
	@go run cmd/api/main.go

# Seed database for teting
seed:
	@go run cmd/seeder/main.go

# Clean the binary
clean:
	@echo "Cleaning..."
	@rm -f main

# Live Reload
watch:
	@if command -v air > /dev/null; then \
	    air; \
	    echo "Watching...";\
	else \
	    read -p "Go's 'air' is not installed on your machine. Do you want to install it? [Y/n] " choice; \
	    if [ "$$choice" != "n" ] && [ "$$choice" != "N" ]; then \
	        go install github.com/air-verse/air@latest; \
	        air; \
	        echo "Watching...";\
	    else \
	        echo "You chose not to install air. Exiting..."; \
	        exit 1; \
	    fi; \
	fi

# Test the application
test:
	@echo "Testing..."
	@go test ./tests -v | sed ''/PASS/s//$$(printf "\033[32mPASS\033[0m")/'' | sed ''/FAIL/s//$$(printf "\033[31mFAIL\033[0m")/''

# Run database on host network
db:
	@docker run -d --rm --name=db-host --network=host \
		-v db-host:/var/lib/mysql \
		-e MYSQL_ROOT_PASSWORD=root \
		-e MYSQL_DATABASE=${DB_NAME} \
		-e MYSQL_USER=${DB_USER} \
		-e MYSQL_PASSWORD=${DB_PASS} \
		mysql:latest

mysql:
	@docker exec -it db-host mysql -uroot -proot ${DB_NAME}

# Migrate db schema
schema:
	@cat ./schema.sql | docker exec -i db-host mysql -uroot -proot ${DB_NAME}

.PHONY: all build run seed test db mysql schema
