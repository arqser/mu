package main

import (
	"log"
	"mu/internal/database"
	"mu/internal/task"
	"time"

	"github.com/brianvoe/gofakeit/v7"
	"golang.org/x/sync/errgroup"
)

type Seeder struct {
	db database.Service
}

func NewSeeder() Seeder {
	s := Seeder{
		db: database.New(),
	}

	return s
}

func main() {
	s := NewSeeder()
	log.Printf("Seeding ...")

	// The order of tables must be precise depending on foreign keys
	// The records within tables can be written concurrently
	s.createTasks(1000)

	log.Printf("Done")
}

func (s *Seeder) createTasks(count int) {
	var t task.Task
	err := s.db.DeleteTbl(&t)
	if err != nil {
		log.Fatalf(err.Error())
	}

	eg := errgroup.Group{}
	for i := 1; i <= count; i++ {
		eg.Go(func() error {
			var t task.Task
			t.Title = gofakeit.Phrase()
			t.Content = gofakeit.Paragraph(1, 3, 27, " ")
			t.CreatedAt = time.Now().Format("2006-01-02 15:04:05")
			t.ID = int64(i)
			err := s.db.Insert(&t)
			if err != nil {
				return err
			}

			return nil
		})
	}

	if err := eg.Wait(); err != nil {
		log.Fatalf(err.Error())
	}
}
