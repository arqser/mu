# Build app
FROM golang:1.22.4
WORKDIR /src
COPY . .
ENV CGO_ENABLED=0
RUN go build -o app cmd/api/main.go

# Deploy app into small image
FROM scratch
WORKDIR /src
COPY --from=0 /src/.env /src/app ./

CMD ["/src/app"]
