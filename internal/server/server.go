package server

import (
	"fmt"
	"net/http"
	"os"
	"time"

	_ "github.com/joho/godotenv/autoload"

	"mu/internal/task"
)

type Server struct {
	port string
	addr string

	t task.TaskHandler
}

func NewServer() *http.Server {
	NewServer := &Server{
		port: os.Getenv("PORT"),
		addr: os.Getenv("BIND_ADDR"),

		t: task.NewTaskHandler(),
	}

	// Declare Server config
	server := &http.Server{
		Addr:         fmt.Sprintf("%v:%v", NewServer.addr, NewServer.port),
		Handler:      NewServer.RegisterRoutes(),
		IdleTimeout:  time.Minute,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 30 * time.Second,
	}

	return server
}
