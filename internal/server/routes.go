package server

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

func (s *Server) RegisterRoutes() http.Handler {
	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.AllowContentType("application/json"))
	r.Use(HandleCors)

	r.Post("/tasks", s.t.Create)
	r.Get("/tasks/{id}", s.t.Show)
	r.Get("/tasks", s.t.List)
	r.Put("/tasks/{id}", s.t.Edit)
	r.Delete("/tasks/{id}", s.t.Delete)

	return r
}
