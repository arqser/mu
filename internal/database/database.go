package database

import (
	"database/sql"
	"fmt"
	"log"
	"net/url"
	"os"
	"reflect"
	"slices"
	"strconv"
	"strings"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/joho/godotenv/autoload"
)

// Tabler represents a MySQL table
//
// To include a field in a simple LIKE-based search use struct tag mu:"search"
// To enable filtering on a field via url params set mu:"filter"
type Tabler interface {
	TableName() string
}

// Service represents a service that interacts with a database.
type Service interface {

	// Close terminates the database connection.
	// It returns an error if the connection cannot be closed.
	Close() error

	// DeleteTbl erases entire table
	DeleteTbl(t Tabler) error

	// Insert persists a single record
	Insert(t Tabler) error

	// Update persists changes on a single record
	Update(t Tabler, id int64) error

	// Find binds a single record
	Find(t Tabler, id int64) error

	// Select returns multiple records
	//
	// To paginate result use url params page and page_size
	// To search use url param search
	// To filter a single column use url params like ?column=value
	// To sort by a certain column use for example ?order_by=id,desc
	Select(t Tabler, url url.Values) ([]Tabler, error)
}

type service struct {
	db *sql.DB
}

var (
	dbname     = os.Getenv("DB_NAME")
	password   = os.Getenv("DB_PASS")
	username   = os.Getenv("DB_USER")
	port       = os.Getenv("DB_PORT")
	host       = os.Getenv("DB_HOST")
	dbInstance *service
)

func New() Service {
	// Reuse Connection
	if dbInstance != nil {
		return dbInstance
	}

	// Opening a driver typically will not attempt to connect to the database.
	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", username, password, host, port, dbname))
	if err != nil {
		// This will not be a connection error, but a DSN parse error or
		// another initialization error.
		log.Fatal(err)
	}
	db.SetConnMaxLifetime(0)
	db.SetMaxIdleConns(50)
	db.SetMaxOpenConns(50)

	dbInstance = &service{
		db: db,
	}
	return dbInstance
}

// Close closes the database connection.
// It logs a message indicating the disconnection from the specific database.
// If the connection is successfully closed, it returns nil.
// If an error occurs while closing the connection, it returns the error.
func (s *service) Close() error {
	log.Printf("Disconnected from database: %s", dbname)
	return s.db.Close()
}

func hasStructTag(t Tabler, tag string) bool {
	v := reflect.TypeOf(t).Elem()
	for i := 0; i < v.NumField(); i++ {
		if tag == v.Field(i).Tag.Get("json") {
			return true
		}
	}

	return false
}

func getStructTags(t Tabler) []string {
	v := reflect.TypeOf(t).Elem()
	tags := make([]string, v.NumField())
	for i := 0; i < v.NumField(); i++ {
		tags[i] = v.Field(i).Tag.Get("json")
	}

	return tags
}

// option refers to custom struct tag values on t
func getStructTagsFromOption(t Tabler, option string) []string {
	v := reflect.TypeOf(t).Elem()
	var tags []string
	for i := 0; i < v.NumField(); i++ {
		if customTag := v.Field(i).Tag.Get("mu"); strings.Contains(customTag, option) {
			tags = append(tags, v.Field(i).Tag.Get("json"))
		}
	}

	return tags
}

func getStructVals(t Tabler) []any {
	v := reflect.ValueOf(t).Elem()
	vals := make([]any, v.NumField())
	for i := 0; i < v.NumField(); i++ {
		vals[i] = v.Field(i).Addr().Interface()
	}

	return vals
}

func (s *service) Insert(t Tabler) error {
	tags := getStructTags(t)
	clause := strings.Join(tags, ",")
	fillQuestionMarks := strings.Repeat("?, ", len(tags)-1)
	query := fmt.Sprintf("INSERT INTO %v (%v) VALUES (%v?)", t.TableName(), clause, fillQuestionMarks)
	res, err := s.db.Exec(query, getStructVals(t)...)
	if err != nil {
		return err
	}
	id, _ := res.LastInsertId()
	if err := s.Find(t, id); err != nil {
		return err
	}

	return nil
}

func (s *service) Find(t Tabler, id int64) error {
	fields := strings.Join(getStructTags(t), ",")
	query := fmt.Sprintf("SELECT %v FROM %v WHERE id = ?", fields, t.TableName())
	if err := s.db.QueryRow(query, id).Scan(getStructVals(t)...); err != nil {
		return err
	}

	return nil
}

func (s *service) Update(t Tabler, id int64) error {
	fields := strings.Join(getStructTags(t), " = ?, ") + " = ? "
	query := fmt.Sprintf("UPDATE %v SET %v WHERE id = %v", t.TableName(), fields, id)
	_, err := s.db.Exec(query, getStructVals(t)...)
	if err != nil {
		return err
	}

	return nil
}

var (
	clause = make([]string, 0)
	vals   = make([]any, 0)
)

func (s *service) Select(t Tabler, url url.Values) ([]Tabler, error) {
	clause = nil
	vals = nil
	// The order of the following helpers matters because of the MySQL syntax
	// They add unto the query various conditions based on URL params
	withFilter(url, t)
	withSearch(url, getStructTagsFromOption(t, "search"))
	withSort(url, t)
	withPagination(url)

	fields := strings.Join(getStructTags(t), ",")
	query := fmt.Sprintf("SELECT %v FROM %v WHERE id > 0 %v", fields, t.TableName(), strings.Join(clause, " "))

	rows, err := s.db.Query(query, vals...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var res []Tabler
	for rows.Next() {
		if err := rows.Scan(getStructVals(t)...); err != nil {
			return nil, err
		}

		// Dereference interface pointer and create new instance of underlying type
		new := reflect.New(reflect.TypeOf(t).Elem())
		val := reflect.ValueOf(t).Elem()
		new.Elem().Set(val)
		res = append(res, new.Interface().(Tabler))
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return res, nil
}

func withFilter(url url.Values, t Tabler) {
	tags := getStructTagsFromOption(t, "filter")

	for filter, val := range url {
		if hasStructTag(t, filter) && slices.Contains(tags, filter) {

			clause = append(clause, fmt.Sprintf("AND %v LIKE ?", filter))
			vals = append(vals, fmt.Sprintf("%%%v%%", val[0]))
		}
	}
}

// withSearch() adds LIKE operators to WHERE clause based on custom struct tags and url search param
func withSearch(url url.Values, tags []string) {
	search := url.Get("search")
	if len(tags) < 1 || len(search) < 1 {
		return
	}

	c := fmt.Sprintf("AND (%v LIKE ?", tags[0])
	for i, tag := range tags {
		if i > 0 {
			c += fmt.Sprintf(" OR %v LIKE ?", tag)
		}
		vals = append(vals, strings.ReplaceAll(fmt.Sprintf("%%%v%%", search), "\"", ""))
	}
	clause = append(clause, fmt.Sprintf("%v)", c))
}

func withSort(url url.Values, t Tabler) {
	if !url.Has("order_by") {
		return
	}
	arr := strings.Split(url.Get("order_by"), ",")
	if !hasStructTag(t, arr[0]) {
		return
	}

	clause = append(clause, " ORDER BY "+arr[0])
	if len(arr) > 1 {
		if arr[1] == "desc" {
			clause = append(clause, " DESC")
		}
	}
}

func withPagination(url url.Values) {
	page, _ := strconv.Atoi(url.Get("page"))
	if page <= 0 {
		page = 1
	}

	limit, _ := strconv.Atoi(url.Get("page_size"))
	switch {
	case limit > 100:
		limit = 100
	case limit <= 0:
		limit = 10
	}

	clause = append(clause, " LIMIT ? OFFSET ?")
	vals = append(vals, limit, (page-1)*limit)
}

func (s *service) DeleteTbl(t Tabler) error {
	if _, err := s.db.Exec("DELETE FROM " + t.TableName()); err != nil {
		return err
	}

	if _, err := s.db.Exec("ALTER TABLE " + t.TableName() + " AUTO_INCREMENT = 0"); err != nil {
		return err
	}

	return nil
}
