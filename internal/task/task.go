package task

import (
	"database/sql"
	"encoding/json"
	"errors"
	"mu/internal/database"
	"net/http"
	"reflect"
	"strconv"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-playground/validator/v10"
)

type Task struct {
	ID        int64   `json:"id"`
	Title     string  `json:"title" validate:"required,ascii" mu:"search"`
	Content   string  `json:"content" validate:"required,ascii" mu:"search"`
	IsDone    bool    `json:"is_done" validate:"boolean" mu:"filter"`
	CreatedAt string  `json:"created_at"`
	DeletedAt *string `json:"deleted_at"`
}

func (t *Task) TableName() string {
	return "tasks"
}

func NewTask(t *TaskRequest) *Task {
	task := &Task{
		Title:     t.Title,
		Content:   t.Content,
		IsDone:    false,
		CreatedAt: time.Now().Format("2006-01-02 15:04:05"),
	}

	return task
}

type omit *struct{}

type TaskRequest struct {
	*Task
	ID        omit `json:"id,omitempty"`
	IsDone    omit `json:"is_done,omitempty"`
	CreatedAt omit `json:"created_at,omitempty"`
	DeletedAt omit `json:"deleted_at,omitempty"`
}

type TaskResponse struct {
	*Task
	DeletedAt omit `json:"deleted_at,omitempty"`
}

func NewTaskResponse(t *Task) *TaskResponse {
	resp := &TaskResponse{
		Task: &Task{
			ID:        t.ID,
			Title:     t.Title,
			Content:   t.Content,
			IsDone:    t.IsDone,
			CreatedAt: t.CreatedAt,
		},
	}

	return resp
}

func NewTaskResponseList(tasks []database.Tabler) []*TaskResponse {
	var list []*TaskResponse
	for _, task := range tasks {
		task := reflect.ValueOf(task).Interface().(*Task)
		resp := NewTaskResponse(task)

		list = append(list, resp)
	}

	return list
}

type TaskHandler struct {
	db database.Service
	v  *validator.Validate
}

func NewTaskHandler() TaskHandler {
	t := TaskHandler{
		db: database.New(),
		v:  validator.New(),
	}

	return t
}

func (h *TaskHandler) Create(w http.ResponseWriter, r *http.Request) {
	var data TaskRequest
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Validate user input
	if err := h.v.Struct(data); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	t := NewTask(&data)
	if err := h.db.Insert(t); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json, err := json.Marshal(NewTaskResponse(t))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	_, _ = w.Write(json)
}

func (h *TaskHandler) Show(w http.ResponseWriter, r *http.Request) {
	id, _ := strconv.Atoi(chi.URLParam(r, "id"))
	var t Task
	if err := h.db.Find(&t, int64(id)); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			http.Error(w, err.Error(), http.StatusNotFound)
			return
		}
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json, err := json.Marshal(NewTaskResponse(&t))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	_, _ = w.Write(json)
}

func (h *TaskHandler) List(w http.ResponseWriter, r *http.Request) {
	var t Task
	url := r.URL.Query()
	res, err := h.db.Select(&t, url)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json, err := json.Marshal(NewTaskResponseList(res))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	_, _ = w.Write(json)
}

func (h *TaskHandler) Edit(w http.ResponseWriter, r *http.Request) {
	// Find record
	id, _ := strconv.Atoi(chi.URLParam(r, "id"))
	var t Task
	if err := h.db.Find(&t, int64(id)); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err := json.NewDecoder(r.Body).Decode(&t)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Validate user input
	if err := h.v.Struct(t); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = h.db.Update(&t, t.ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json, err := json.Marshal(NewTaskResponse(&t))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	_, _ = w.Write(json)
}

func (h *TaskHandler) Delete(w http.ResponseWriter, r *http.Request) {
	// Find record
	id, _ := strconv.Atoi(chi.URLParam(r, "id"))
	var t Task
	if err := h.db.Find(&t, int64(id)); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	now := time.Now().Format("2006-01-02 15:04:05")
	t.DeletedAt = &now
	err := h.db.Update(&t, t.ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
	_, _ = w.Write(nil)
}
